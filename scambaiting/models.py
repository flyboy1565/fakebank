from django.db import models

from phonenumber_field.modelfields import PhoneNumberField


def Pushiness_Choices():
    return (
        ('Unknown', 'Unknown'),
        ('Not', 'Not Pushy'),
        ('Kinda', 'Kinda Pushy'),
        ('Pushy', 'Pushy'),
        ('Assholes', 'Assholes'),
        ('Sell', 'Sell Their Gmas For a Buck'),
    )


class PhoneSource(models.Model):
    text = models.CharField(max_length=100, unique=True)
    
    def __str__(self):
        return self.text
  
    
class ScamType(models.Model):
    text = models.CharField(max_length=50, unique=True)
    
    def __str__(self):
        return self.text    


class Contact(models.Model):
    phone_number = PhoneNumberField(unique=True)
    source = models.ForeignKey(PhoneSource, on_delete=models.CASCADE)
    url = models.URLField(max_length=300)
    added_ts = models.DateTimeField(auto_now_add=True)
    updated_ts = models.DateTimeField(auto_now=True)
    scam_type = models.ForeignKey(ScamType, on_delete=models.CASCADE)
    scam_specific_notes = models.TextField()
    tollfree = models.BooleanField(default=True)
    pushy = models.CharField(choices=Pushiness_Choices(), max_length=20, default="Unknown")
    
    def __str__(self):
        return str(self.phone_number)
    
    def note_count(self):
        return self.contactnote_set.count()
        
    
class ContactNote(models.Model):
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE)
    note = models.TextField()
    added_ts = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.note[:30]
    