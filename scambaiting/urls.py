from django.urls import path, include

from . import views

app_name = 'scambaiting'

urlpatterns = [
    path('', views.index, name='index'),
    path('contact/new', views.add_contact, name='addContact'),
    path('contact/<int:contactID>', views.add_note, name="addNote"),
]