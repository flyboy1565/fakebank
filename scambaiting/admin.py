from django.contrib import admin

from .models import *


class ContactAdmin(admin.ModelAdmin):
    list_display = ['phone_number', 'source', 'added_ts', 'updated_ts', 'scam_type', 'tollfree', 'pushy']
    list_filter = ['source', 'scam_type']

admin.site.register(PhoneSource)
admin.site.register(ScamType)
admin.site.register(Contact, ContactAdmin)
admin.site.register(ContactNote)