from decimal import Decimal
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, Group
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse, reverse_lazy
from django.conf import settings

from .forms import ContactForm, AddContactForm
from .models import Contact

def index(request):
    contacts = Contact.objects.all()
    return render(request, "baiting_index.html", {'contacts': contacts})

@login_required
def add_note(request, contactID):
    contact = Contact.objects.get(pk=contactID)
    notes = contact.contactnote_set.select_related().order_by('-pk')
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save(commit=False)
            form.instance.contact = contact
            form.save()
            url = reverse('scambaiting:addNote', kwargs={'contactID': contactID})
            return HttpResponseRedirect(url)
    else:
        form = ContactForm()
    context = {'contact': contact, 'notes': notes, 'form': form}
    return render(request, "add_note.html", context)


@login_required
def add_contact(request):
    if request.method == 'POST':
        form = AddContactForm(request.POST)
        if form.is_valid():
            form.save()
            url = reverse('scambaiting:index')
            return HttpResponseRedirect(url)
    else:
        form = AddContactForm()
    return render(request, "add_contact.html", {'form': form})