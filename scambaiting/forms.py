from django import forms
from django.core.exceptions import ValidationError
from django.forms import ModelForm, TextInput
from .models import *


class ContactForm(ModelForm):
    
    class Meta:
        model = ContactNote
        fields = ['note']
        widgets = {
            'note': TextInput()
        }
        
    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
            

class AddContactForm(forms.ModelForm):
    
    class Meta:
        model = Contact
        fields = ('phone_number', 'source', 'url', 'scam_type', 'scam_specific_notes', 'tollfree', 'pushy')
