from django.urls import path, include

from . import views

app_name = 'customer'

urlpatterns = [
    path('', views.index, name='index'),
    path('profile', views.profile, name="profile"),
    path('accounts/<int:account_id>', views.account_activity, name='account_activity'),
    path('accounts/', views.accounts, name='accounts'),
    path('transfer', views.transfer, name='transfer'),
]