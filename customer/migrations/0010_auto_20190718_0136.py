# Generated by Django 2.2.3 on 2019-07-18 01:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0009_auto_20190717_2257'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customer',
            name='account_no',
        ),
        migrations.RemoveField(
            model_name='customer',
            name='balance',
        ),
    ]
