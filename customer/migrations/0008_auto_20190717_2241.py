# Generated by Django 2.2.3 on 2019-07-17 22:41

from django.db import migrations
import localflavor.us.models
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0007_auto_20190717_2240'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='phone',
            field=phonenumber_field.modelfields.PhoneNumberField(blank=True, max_length=128, null=True, region=None),
        ),
        migrations.AlterField(
            model_name='customer',
            name='state',
            field=localflavor.us.models.USStateField(blank=True, max_length=2, null=True),
        ),
        migrations.AlterField(
            model_name='customer',
            name='zip_code',
            field=localflavor.us.models.USZipCodeField(blank=True, max_length=10, null=True),
        ),
    ]
