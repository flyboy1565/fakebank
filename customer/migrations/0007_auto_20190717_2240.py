# Generated by Django 2.2.3 on 2019-07-17 22:40

from django.db import migrations, models
import localflavor.us.models
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0006_auto_20171025_1331'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='zip_code',
            field=localflavor.us.models.USZipCodeField(default=93465, max_length=10),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='customer',
            name='country',
            field=models.CharField(default='USA', max_length=50),
        ),
        migrations.AlterField(
            model_name='customer',
            name='phone',
            field=phonenumber_field.modelfields.PhoneNumberField(max_length=128, region=None),
        ),
        migrations.AlterField(
            model_name='customer',
            name='state',
            field=localflavor.us.models.USStateField(max_length=2),
        ),
    ]
