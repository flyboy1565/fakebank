from django.contrib import admin

from .models import Customer

class CustomerAdmin(admin.ModelAdmin):
    list_display = ['user', 'name', 'phone', 'birthday', 'city', 'state', 'country', 'zip_code']
        

admin.site.register(Customer, CustomerAdmin)