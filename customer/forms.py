from django import forms
from django.core.exceptions import ValidationError
from django.forms import ModelForm
from .models import *

from transaction.models import Transfer

class CustomerForm(ModelForm):

    class Meta:
        model = Customer
        fields=['name','birthday','phone','street','city','state','country', 'zip_code', 'pin_code']
        

class TransferForm(ModelForm):
    
    class Meta:
        model = Transfer
        fields = ['from_account', 'to_account', 'amount']
        
    def check_funds_able(self, fromValue, toValue, amount):
        print('*************************')
        print('FromValue:', fromValue)
        print('toValue:', toValue)
        print('amount:', amount)
        print('Value Check:', (fromValue - amount) < 0)
        if (fromValue - amount) < 0:
            # self._errors['amount'] = 'Insufficant Funds to Transfer.'
            raise forms.ValidationError('Insufficant Funds to Transfer.')
        return True
    
    def clean_data_get(self, field):
        data = self.cleaned_data.get(field)        
        return data

    def clean(self):
        super(TransferForm, self).clean()
        from_account = self.clean_data_get("from_account")
        to_account = self.clean_data_get("to_account")
        amount = self.clean_data_get("amount")
        self.check_funds_able(from_account.balance, to_account.balance, amount)
        
        