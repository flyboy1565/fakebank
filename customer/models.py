import uuid

from decimal import Decimal
from django.db import models
from django.contrib.auth.models import User
from localflavor.us.models import USZipCodeField, USStateField
from phonenumber_field.modelfields import PhoneNumberField

# Create your models here.
class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=200,null=True)
    birthday = models.DateField()
    phone = PhoneNumberField(null=True, blank=True)  
    street = models.CharField(max_length=200,null=True)
    city = models.CharField(max_length=200,null=True)
    state = USStateField(null=True, blank=True)
    country = models.CharField(max_length=50, default="USA")
    zip_code = USZipCodeField(null=True, blank=True)
    pin_code = models.IntegerField(null=True)

    def __str__(self):
        return str(self.user)