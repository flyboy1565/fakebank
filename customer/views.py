from decimal import Decimal
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, Group
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse, reverse_lazy
from django.conf import settings

from .forms import CustomerForm, TransferForm
from .models import Customer
from transaction.models import Transaction, Account
# Create your views here.

def index(request):
    if request.user.groups.filter(name='Employee').exists():
        return HttpResponseRedirect(reverse('employee:index'))
    return render(request, "index.html", {'bank_name': settings.BANKING_SYSTEM_NAME})

@login_required
def accounts(request):
    user = Customer.objects.get(user=request.user)
    accounts = user.account_set.select_related()
    print(accounts)
    context = {'bank_name': settings.BANKING_SYSTEM_NAME, 'accounts': accounts, 'p': user}
    return render(request, "accounts.html", context)

@login_required
def account_activity(request, account_id):
    user = Customer.objects.get(user=request.user)
    account = Account.objects.get(user=user, account_id=account_id)
    transactions = account.transaction_set.select_related().order_by('-transaction_timestamp')
    context = {
        'bank_name': settings.BANKING_SYSTEM_NAME, 
        'transactions': transactions, 
        'account': account,
        'p': user
    }
    return render(request, "account_activity.html", context)

@login_required
def transfer(request):
    user = Customer.objects.get(user=request.user)
    if request.method == 'POST':
        form = TransferForm(request.POST)
        if form.is_valid():
            form.save(commit=False)
            form.instance.user = user
            form.save()
            return HttpResponseRedirect(reverse('customer:accounts'))
    else:
        form = TransferForm()
    context = {
        'bank_name': settings.BANKING_SYSTEM_NAME, 
        'form': form,
    }
    return render(request, "transfer.html", context)


@login_required
def profile(request):
    if request.method == 'POST':
        customer = Customer.objects.get(user=request.user)
        form = CustomerForm(request.POST, instance=customer)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('customer:accounts'))
    else:
        customer = Customer.objects.get(user=request.user)
        form = CustomerForm(instance=customer)
    context = {
        'bank_name': settings.BANKING_SYSTEM_NAME, 
        'form': form,
    }
    return render(request, "profile.html", context)