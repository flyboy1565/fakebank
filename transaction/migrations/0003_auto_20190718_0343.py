# Generated by Django 2.2.3 on 2019-07-18 03:43

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('transaction', '0002_auto_20190718_0136'),
    ]

    operations = [
        migrations.AddField(
            model_name='transfer',
            name='transfer_id',
            field=models.CharField(default=uuid.uuid4, editable=False, max_length=100, unique=True),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='transaction_id',
            field=models.CharField(default=uuid.uuid4, editable=False, max_length=100, unique=True),
        ),
    ]
