from django.contrib import admin

# Register your models here.
from .models import Transaction, Account, Transfer

class TransactionAdmin(admin.ModelAdmin):
    list_display = ['user', 'type', 'transaction_id', 'notes', 'transaction_timestamp', 'previous_balance', 'amount', 'current_balance']
    list_filter = ['user',]
    

class AccountAdmin(admin.ModelAdmin):
    list_display = ['user', 'account_type', 'account_id', 'balance']    
    list_filter = ['user',]
    
    
class TransferAdmin(admin.ModelAdmin):
    list_display = ['user', 'from_account', 'to_account', 'amount']    
    list_filter = ['user',]

admin.site.register(Transfer, TransferAdmin)
admin.site.register(Account, AccountAdmin)
admin.site.register(Transaction, TransactionAdmin)