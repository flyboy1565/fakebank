from customer.models import Customer
from django.db import models
import datetime
from django.utils import timezone
from uuid import uuid4

W = "Withdrawal"
D = "Deposit"
T = "Account Transfer"
P = "Purchase"
O = "Overdraft"

def TransactionChoices():
    return ( (W, W), (D, D) )

def AccountChoices():
    return (
        ("Savings", "Savings"),
        ("Checking", "Checking"),
        ("Credit", "Credit"),
        ("Personal Loan", "Personal Loan"),
        ("Car Loan", "Car Loan"),
        ("Mortage Loan", "Mortage Loan"),
    )

class Account(models.Model):
    user = models.ForeignKey(Customer, on_delete=models.CASCADE)
    account_type = models.CharField(choices=AccountChoices(), max_length=50)
    type = models.CharField(choices=(('P', 'Personal'), ('L', 'Loans')), max_length=5)
    account_id = models.PositiveIntegerField()
    balance = models.DecimalField(max_digits=20,decimal_places=2, editable=False, default=0)
    
    def __str__(self):
        return "{} - {} - {}".format(
            self.user.user.username, 
            self.account_type,
            self.account_id
        )
        

class Transaction(models.Model):
    previous_balance = models.DecimalField(max_digits=20,decimal_places=2, editable=False)
    current_balance = models.DecimalField(max_digits=20,decimal_places=2, editable=False)
    transaction_timestamp = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(Customer, on_delete=models.CASCADE)
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=20,decimal_places=2)
    transaction_id = models.CharField(max_length=100, editable=False, unique=True, default=uuid4)
    type = models.CharField(max_length=50,choices=TransactionChoices())
    notes = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return "{}".format(self.transaction_id)

    def get_transaction_id(self):
        trans=str(self.user.username)+'_'+str(self.pk)
        return trans
    
    def save(self, *args, **kwargs):
        account = Account.objects.get(pk=self.account.pk)
        print('Working on Account: ', account.account_id)
        self.previous_balance = self.account.balance
        if self.type == W:
            account.balance -= self.amount
            print("Withdrawl:", self.amount)
        elif self.type == D:
            account.balance += self.amount
            print("Deposit:", self.amount)
        account.save()
        print('New Balance: ', account.balance)
        self.current_balance = account.balance
        print('Current Balance:', self.current_balance)
        super(Transaction, self).save(*args, **kwargs)
    
        
class Transfer(models.Model):
    user = models.ForeignKey(Customer, on_delete=models.CASCADE)
    from_account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name="transfer_from")
    to_account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name="transfer_to")
    transfer_id = models.CharField(max_length=100, editable=False, unique=True, default=uuid4)
    amount = models.PositiveIntegerField()
    
    def __str__(self):
        return "{}".format(self.transfer_id)
               
    def save(self, *args, **kwargs):
        fromAccount = Transaction()
        fromAccount.user = self.user
        fromAccount.account = self.from_account
        fromAccount.type = W
        fromAccount.amount = self.amount
        fromAccount.notes = "Transfer to Account: {}".format(self.to_account.account_id)
        fromAccount.save()
        toAccount = Transaction()
        toAccount.user = self.user
        toAccount.account = self.to_account
        toAccount.amount = self.amount
        toAccount.type = D
        toAccount.notes = "Transfer from Account: {}".format(self.from_account.account_id)
        toAccount.save()
        super(Transfer, self).save(*args, **kwargs)
    